export const environment = {
  production: true,
  name: 'prod',
  apiKey: process.env.API_KEY,
  apiReadAccessToken: process.env.API_READ_ACCESS_TOKEN,
  apiVersion: process.env.API_VERSION
};
