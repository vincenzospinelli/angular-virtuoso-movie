export const config = {
  apiBaseUrl: 'https://api.themoviedb.org/',
  imageBaseUrl: 'https://image.tmdb.org/t/p/w500',
  imageDefaultUrlCard: 'https://via.placeholder.com/500x750/001529/FFF?text=VIRTUOSO-MOVIE'
};
