import {NgModule} from '@angular/core';
import {MovieSearchComponent} from './movie-search.component';
import {SharedModule} from '../../../../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', component: MovieSearchComponent}
];

@NgModule({
  declarations: [
    MovieSearchComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MovieSearchModule {
}
