import {Component, OnDestroy, OnInit} from '@angular/core';
import {TheMovieDbService} from '../../../../services/the-movie-db.service';
import {gridResponsiveMap, NzBreakpointEnum, NzBreakpointService} from 'ng-zorro-antd/core/services';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {delay, tap} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {MovieModel} from '../../../../models/movie.model';
import {LanguageService} from '../../../../services/language.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-movie-search',
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.scss']
})
export class MovieSearchComponent implements OnInit, OnDestroy {

  movies: MovieModel[] = [];
  page: number = 1;
  loader: boolean = false;
  loaderPage: boolean = true;
  progressWidth: number;
  query: string;
  lang: string;
  sizePaginator: string;
  sub: Subscription;

  constructor(
    private tmdbService: TheMovieDbService,
    private breakpointService: NzBreakpointService,
    private translate: TranslateService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.layout();

    this.sub = this.languageService.getMessage().subscribe(flag => {
      if (flag) {
        this.lang = this.translate.currentLang;
        this.router.navigate([], {relativeTo: this.route, queryParams: {lang: this.lang}, queryParamsHandling: 'merge'});
        this.ngOnInit();
      }
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.loaderPage = false;
    }, 1000);
    this.query = this.route.snapshot.queryParams.query;
    this.lang = this.translate.currentLang;
    this.searchMovie(this.query, this.page);
  }

  searchMovie(q: string, p: number) {
    this.tmdbService.searchMovie(q, this.lang, p)
      .pipe(
        tap(res => {
          if (res.results.length > 0) {
            this.loaderPage = true;
            this.loader = true;
          }
        }),
        delay(1000))
      .subscribe((res: any) => {
        if (res.results.length > 0) {
          this.movies = [...this.movies, ...res.results];
          this.router.navigate([], {
            relativeTo: this.route,
            queryParams: {page: p, query: q, lang: this.lang},
            queryParamsHandling: 'merge'
          });
          this.loaderPage = false;
          this.loader = false;
        }
      }, error => {
        throw new HttpErrorResponse(error);
      });
  }

  onQuerySearchChange(query: string, page?: number) {
    this.searchMovie(query, page ? page : 1);
  }

  onDeleteSearch() {
    this.query = '';
    this.movies = [];
    this.router.navigate([], {relativeTo: this.route, queryParams: {lang: this.lang}});
  }

  onScrollDown() {
    this.page++;
    this.searchMovie(this.query, this.page);
  }

  private layout() {
    this.breakpointService.subscribe(gridResponsiveMap).subscribe((bp: NzBreakpointEnum) => {
      console.log(bp);
      const p = NzBreakpointEnum.lg;
      switch (bp) {
        case NzBreakpointEnum.sm:
        case NzBreakpointEnum.lg:
        case NzBreakpointEnum.md:
        case NzBreakpointEnum.xs:
          this.progressWidth = 50;
          this.sizePaginator = 'small';
          break;
        case NzBreakpointEnum.xl:
        case NzBreakpointEnum.xxl:
          this.progressWidth = 45;
          this.sizePaginator = 'default';
          break;
      }
    });
  }
}
