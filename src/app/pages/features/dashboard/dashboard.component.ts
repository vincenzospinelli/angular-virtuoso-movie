import {Component, OnDestroy, OnInit} from '@angular/core';
import {TheMovieDbService} from '../../../services/the-movie-db.service';
import {delay, tap} from 'rxjs/operators';
import {gridResponsiveMap, NzBreakpointEnum, NzBreakpointService} from 'ng-zorro-antd/core/services';
import {HttpErrorResponse} from '@angular/common/http';
import {MovieModel} from '../../../models/movie.model';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {LanguageService} from '../../../services/language.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  movies: MovieModel[] = [];
  page: number = 1;
  pageSize: number;
  totalResults: number;
  loader: boolean = false;
  loaderPage: boolean = true;
  progressWidth: number;
  query: string;
  selectedFilter: string = 'popularity.desc';
  lang: string;
  sizePaginator: string;
  sub: Subscription;

  constructor(
    private tmdbService: TheMovieDbService,
    private breakpointService: NzBreakpointService,
    private translate: TranslateService,
    private languageService: LanguageService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.layout();
    this.lang = this.translate.currentLang;
    this.sub = this.languageService.getMessage().subscribe(flag => {
      if (flag) {
        this.lang = this.translate.currentLang;
        this.router.navigate([], {relativeTo: this.route, queryParams: {page: this.page, lang: this.lang}, queryParamsHandling: 'merge'});
        this.getDiscoverMovie(this.page);
      }
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.loaderPage = false;
    }, 2000);
    this.selectedFilter = this.route.snapshot.queryParams.sortBy ? this.route.snapshot.queryParams.sortBy : 'popularity.desc';
    this.onPageIndexChange(this.page);
  }

  onPageIndexChange(page: any) {
    this.getDiscoverMovie(page);
  }

  onSelectedChange(page: number) {
    this.onPageIndexChange(page);
  }

  disabledFilter(): boolean {
    if (!this.query) {
      return false;
    }

    return true;
  }

  goToSearchMoviePage(q: string) {
    this.router.navigate(['/dashboard/search-movie'], {queryParams: {query: q}});
  }

  onKeyDown(e: KeyboardEvent) {
    this.goToSearchMoviePage(this.query);
  }

  private getDiscoverMovie(page: any) {
    this.tmdbService
      .getDiscoverMovie(page, this.selectedFilter, this.lang)
      .pipe(
        tap((val: any) => {
          this.loader = true;

          this.page = val.page;
          this.pageSize = val.results.length;
          this.totalResults = val.total_results;

          this.router.navigate(['/dashboard'], {queryParams: {page: this.page, sortBy: this.selectedFilter, lang: this.lang}});
        }),
        delay(1000))
      .subscribe((res: any) => {
        this.movies = res.results;

        this.loader = false;
      }, error => {
        throw new HttpErrorResponse(error);
      });
  }

  private layout() {
    this.breakpointService.subscribe(gridResponsiveMap).subscribe((bp: NzBreakpointEnum) => {
      console.log(bp);
      const p = NzBreakpointEnum.lg;
      switch (bp) {
        case NzBreakpointEnum.sm:
        case NzBreakpointEnum.lg:
        case NzBreakpointEnum.md:
        case NzBreakpointEnum.xs:
          this.progressWidth = 50;
          this.sizePaginator = 'small';
          break;
        case NzBreakpointEnum.xl:
        case NzBreakpointEnum.xxl:
          this.progressWidth = 45;
          this.sizePaginator = 'default';
          break;
      }
    });
  }
}
