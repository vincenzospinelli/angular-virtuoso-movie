import {NgModule} from '@angular/core';
import {SharedModule} from '../../../shared/shared.module';
import {DashboardComponent} from './dashboard.component';
import {BaseDetailsComponent} from './base-details/base-details.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', component: DashboardComponent},
  {
    path: 'search-movie',
    loadChildren: () => import('./movie-search/movie-search.module').then(m => m.MovieSearchModule)
  },
  {
    path: 'movie-details',
    component: BaseDetailsComponent,
    children: [
      {
        path: ':id',
        loadChildren: () => import('./movie-details/movie-details.module').then(m => m.MovieDetailsModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    DashboardComponent,
    BaseDetailsComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DashboardModule {
}
