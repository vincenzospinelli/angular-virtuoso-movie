import {NgModule} from '@angular/core';
import {SharedModule} from '../../../../shared/shared.module';
import {MovieDetailsComponent} from './movie-details.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [{
  path: '', component: MovieDetailsComponent
}];

@NgModule({
  declarations: [
    MovieDetailsComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class MovieDetailsModule {
}
