import {Component, OnInit} from '@angular/core';
import {TheMovieDbService} from '../../../../services/the-movie-db.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {forkJoin, Subscription} from 'rxjs';
import {delay, tap} from 'rxjs/operators';
import {MovieDetailsModel} from '../../../../models/movie-details.model';
import {CastModel} from '../../../../models/cast.model';
import {gridResponsiveMap, NzBreakpointEnum, NzBreakpointService} from 'ng-zorro-antd/core/services';
import {TranslateService} from '@ngx-translate/core';
import {LanguageService} from '../../../../services/language.service';
import {config} from '../../../../app.config';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {

  idMovie: number;
  movieDetails: MovieDetailsModel;
  casts: CastModel[] = [];
  loaderPage: boolean = true;
  loader: boolean = false;
  genres: string;
  imageHeader: string = config.imageDefaultUrlCard;
  imageBaseUrl = config.imageBaseUrl;
  imageDefaultUrlCard = config.imageDefaultUrlCard;
  width: number;
  lang: string;
  sub: Subscription;

  constructor(
    private tmdbService: TheMovieDbService,
    private route: ActivatedRoute,
    private breakpointService: NzBreakpointService,
    private languageService: LanguageService,
    private translate: TranslateService,
    private router: Router
  ) {
    this.layout();

    this.route.params.subscribe((params: Params) => {
      this.idMovie = params.id;
      this.lang = this.translate.currentLang;
    });

    this.sub = this.languageService.getMessage().subscribe(flag => {
      if (flag) {
        this.lang = this.translate.currentLang;
        this.router.navigate([], {relativeTo: this.route, queryParams: {lang: this.lang}, queryParamsHandling: 'merge'});
        this.ngOnInit();
      }
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.loaderPage = false;
    }, 2000);
    this.loader = true;
    this.init(this.idMovie);
  }

  private init(idMovie) {
    forkJoin([
        this.tmdbService.getMovieDetailsById(idMovie, this.lang),
        this.tmdbService.getMovieCreditsById(idMovie, this.lang)
      ]
    ).pipe(
      tap(val => {
        this.loader = true;
      }),
      delay(2000)
    )
      .subscribe((res: any) => {
        this.movieDetails = res[0];
        this.genres = res[0].genres.map(el => el.name).join(', ');
        this.casts = res[1].cast;
        this.imageHeader = this.imageBaseUrl + this.movieDetails.backdrop_path;
        this.loader = false;
      }, error => {
        this.loader = false;
        throw new HttpErrorResponse(error);
      });
  }

  private layout() {
    this.breakpointService.subscribe(gridResponsiveMap).subscribe((bp: NzBreakpointEnum) => {
      console.log(bp);
      switch (bp) {
        case NzBreakpointEnum.xxl:
          this.width = 1000;
          break;
        case NzBreakpointEnum.xl:
          this.width = 800;
          break;
        case NzBreakpointEnum.lg:
          this.width = 400;
          break;
        case NzBreakpointEnum.md:
          this.width = 400;
          break;
        case NzBreakpointEnum.sm:
          this.width = 400;
          break;
        case NzBreakpointEnum.xs:
          this.width = 300;
          break;
      }
    });
  }
}
