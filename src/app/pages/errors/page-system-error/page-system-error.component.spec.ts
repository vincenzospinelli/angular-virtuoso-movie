import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSystemErrorComponent } from './page-system-error.component';

describe('PageSystemErrorComponent', () => {
  let component: PageSystemErrorComponent;
  let fixture: ComponentFixture<PageSystemErrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageSystemErrorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSystemErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
