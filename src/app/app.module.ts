import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, LOCALE_ID, NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TheMovieDbService} from './services/the-movie-db.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from './shared/shared.module';
import {en_US, it_IT, NZ_I18N} from 'ng-zorro-antd/i18n';
import {ErrorsHandler} from './shared/utils/errors-handler';
import {NzNotificationService} from 'ng-zorro-antd/notification';
import {TranslateLoader, TranslateModule, TranslateService, TranslateStore} from '@ngx-translate/core';
import {NzModalService} from 'ng-zorro-antd/modal';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import it from '@angular/common/locales/it';
import {registerLocaleData} from '@angular/common';
import {PageNotFoundComponent} from './pages/errors/page-not-found/page-not-found.component';
import {PageSystemErrorComponent} from './pages/errors/page-system-error/page-system-error.component';
import {LanguageService} from './services/language.service';
import {ENV, getEnv} from './services/environment.service';

registerLocaleData(it);

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    PageSystemErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    TranslateModule.forRoot({
      defaultLanguage: 'it',
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient],
      }
    })
  ],
  providers: [
    HttpClient,
    TheMovieDbService,
    NzNotificationService,
    NzModalService,
    TranslateService,
    TranslateStore,
    LanguageService,
    {
      provide: NZ_I18N, useFactory: (localId: string) => {
        switch (localId) {
          case 'en':
            return en_US;
          case 'it':
            return it_IT;
          default:
            return it_IT;
        }
      },
      deps: [LOCALE_ID]
    },
    {provide: ErrorHandler, useClass: ErrorsHandler},
    {provide: ENV, useFactory: getEnv}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
