import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {NzNotificationDataOptions, NzNotificationService} from 'ng-zorro-antd/notification';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ErrorsHandler implements ErrorHandler {
  private static readonly defaultErrorTitle = 'Errore';
  private static readonly chunkFailedMatch = /.*Loading chunk [^\s]+ failed.*/;
  currentLang: string = this.translateService.currentLang;

  constructor(
    private notificationService: NzNotificationService,
    private translateService: TranslateService,
    private injector: Injector
  ) {
  }

  // generic error handling, will handle exceptions and network errors
  // this method is called from angular, not thought to be called maually
  handleError(error: Error | any | HttpErrorResponse) {
    console.log(error);
    if (!navigator.onLine && (error instanceof HttpErrorResponse || ErrorsHandler.chunkFailedMatch.test(error.error.status_message))) {
      // NOTE: this way we guess it is an offline
      this.notifyError(ErrorsHandler.defaultErrorTitle, error.error.status_message);
      return;
    }
    if (error instanceof HttpErrorResponse) {
      const status = (error as HttpErrorResponse).status;
      switch (status) {
        case 0:
          this.notifyError(ErrorsHandler.defaultErrorTitle + ` ${error.status}`, error.error.status_message);
          this.injector.get(Router).navigate(['/'], {replaceUrl: true});
          return;
        case 403:
          this.notifyError(ErrorsHandler.defaultErrorTitle + ` ${error.status}`, error.error.status_message);
          this.injector.get(Router).navigate(['/'], {replaceUrl: true});
          return;
        case 413:
          this.notifyError(ErrorsHandler.defaultErrorTitle + ` ${error.status}`, error.error.status_message);
          this.injector.get(Router).navigate(['/'], {replaceUrl: true});
          return;
        case 422:
          this.notifyError(ErrorsHandler.defaultErrorTitle + ` ${error.status}`, error.error.errors[0]);
          this.injector.get(Router).navigate(['/'], {replaceUrl: true});
          return;
        default:
          break;
      }

      switch (Math.floor(status / 100)) {
        // 4XX family errors
        case 4:
          this.notifyError(ErrorsHandler.defaultErrorTitle + ` ${error.status}`, error.error.status_message);
          this.injector.get(Router).navigate(['/' + this.currentLang + '/404'], {replaceUrl: true});
          return;
        // 5XX family errors
        case 5:
          this.notifyError(ErrorsHandler.defaultErrorTitle + ` ${error.status}`, error.error.status_message);
          this.injector.get(Router).navigate(['/' + this.currentLang + '/500'], {replaceUrl: true});
          return;
        // no errors
        default:
          break;
      }
    } else {
      // NOTE: handle other errors here
    }
  }

  private notifyError(title: string, message: string, args?: any[]) {
    const options: NzNotificationDataOptions<any> = {nzPauseOnHover: true};
    this.notificationService.error(this.translateService.instant(title), this.translateService.instant(message, args), options);
  }
}
