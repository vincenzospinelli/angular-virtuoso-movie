import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MovieCardComponent} from './components/movie-card/movie-card.component';
import {RouterModule} from '@angular/router';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {NzCardModule} from 'ng-zorro-antd/card';
import {NzProgressModule} from 'ng-zorro-antd/progress';
import {NzGridModule} from 'ng-zorro-antd/grid';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {NzPaginationModule} from 'ng-zorro-antd/pagination';
import {FiltersComponent} from './components/filters/filters.component';
import {NzSkeletonModule} from 'ng-zorro-antd/skeleton';
import {NzSelectModule} from 'ng-zorro-antd/select';
import {NzFormModule} from 'ng-zorro-antd/form';
import {NzInputModule} from 'ng-zorro-antd/input';
import {MovieCardDetailsComponent} from './components/movie-card-details/movie-card-details.component';
import {SerieCastCardComponent} from './components/serie-cast-card/serie-cast-card.component';
import {NzCarouselModule} from 'ng-zorro-antd/carousel';
import {IconsProviderModule} from '../icons-provider.module';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzDropDownModule} from 'ng-zorro-antd/dropdown';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {ThousandSuffixesPipe} from './pipes/thousand-suffixes.pipe';
import {NzSpinModule} from 'ng-zorro-antd/spin';
import {LoaderComponent} from './components/loader/loader.component';
import {EmptyComponent} from './components/empty/empty.component';
import {NzEmptyModule} from 'ng-zorro-antd/empty';

const ZorroModule = [
  NzCardModule,
  NzProgressModule,
  NzGridModule,
  NzPaginationModule,
  NzSkeletonModule,
  NzSelectModule,
  NzFormModule,
  NzInputModule,
  NzCarouselModule,
  NzLayoutModule,
  NzMenuModule,
  NzDropDownModule,
  NzButtonModule,
  NzSpinModule,
  NzEmptyModule
];

const PIPES = [
  ThousandSuffixesPipe
];

const COMPONENTS = [
  MovieCardComponent,
  ProgressBarComponent,
  FiltersComponent,
  MovieCardDetailsComponent,
  SerieCastCardComponent,
  LoaderComponent,
  EmptyComponent
];

const DIRECTIVES = [];

const ENTRY = [];

const SHARED = [
  ...COMPONENTS,
  ...DIRECTIVES,
  ...PIPES,
  ...ENTRY
];

@NgModule({
  declarations: [
    ...SHARED
  ],
  imports: [
    CommonModule,
    RouterModule,
    InfiniteScrollModule,
    IconsProviderModule,
    TranslateModule,
    FormsModule,
    ...ZorroModule
  ],
  exports: [
    ...SHARED,
    ...ZorroModule,
    CommonModule,
    IconsProviderModule,
    InfiniteScrollModule,
    TranslateModule,
    FormsModule
  ],
  entryComponents: [
    ...ENTRY
  ],
  providers: [
    ...PIPES
  ]
})
export class SharedModule {
}
