import {Component, Input, OnInit} from '@angular/core';
import {MovieModel} from '../../../models/movie.model';
import {config} from '../../../app.config';

@Component({
  selector: 'app-movie-card',
  template: `
    <nz-card nzHoverable [nzCover]="!loader ? coverTemplate : coverScheleton"
             [nzActions]="[calendar,heart]">
      <ng-template #coverTemplate>
        <img
          [src]="movieDetails.poster_path ? imgBaseUrl + movieDetails.poster_path : imageDefaultUrlCard">
      </ng-template>
      <nz-card-meta
        class="title"
        [nzTitle]="!loader ? movieDetails.title : ''"
      >
      </nz-card-meta>
      <app-progress-bar
        class="progress-circle"
        *ngIf="!loader"
        [percent]="movieDetails.vote_average * 10"
        [width]="progressWidth">
      </app-progress-bar>
      <nz-skeleton
        [nzLoading]="loader"
        [nzTitle]="{ rows: 1, width:125 }"
        [nzParagraph]="false"
        [nzAvatar]="{ shape:'circle' }"
        [nzActive]="true">
        <ng-template #coverScheleton>
          <img alt="logo" [src]="imageDefaultUrlCard"/>
        </ng-template>
      </nz-skeleton>
    </nz-card>

    <ng-template #calendar>
      <i nz-icon nzType="calendar" nzTheme="twotone"></i>
      {{movieDetails && movieDetails.release_date ? (movieDetails.release_date | date: 'MMM/YYYY') : 'MESSAGGI.NON_DISPONIBILE' | translate}}
    </ng-template>
    <ng-template #heart>
      <i nz-icon nzType="heart" nzTheme="twotone" [nzTwotoneColor]="'red'"></i>
      {{movieDetails?.popularity | thousandSuff}}
    </ng-template>
  `,
  styles: [`
    nz-card {
      border-radius: 20px;
      margin-bottom: 20px;
    }

    img {
      border-top-left-radius: 20px;
      border-top-right-radius: 20px;
      padding: 5px;
    }

    .progress-circle {
      position: absolute;
      left: 10px;
      bottom: 80px;
    }

    .title {
      margin-left: 40px;
    }

    :host::ng-deep .ant-card-body {
      height: 65px !important;
    }

    :host::ng-deep .ant-card-actions {
      border-bottom-left-radius: 20px;
      border-bottom-right-radius: 20px;
    }
  `]
})
export class MovieCardComponent implements OnInit {
  @Input() movieDetails: MovieModel;
  @Input() loader: boolean;
  @Input() progressWidth: number;

  imgBaseUrl: string = config.imageBaseUrl;
  imageDefaultUrlCard: string = config.imageDefaultUrlCard;

  constructor() {
  }

  ngOnInit() {
  }

}
