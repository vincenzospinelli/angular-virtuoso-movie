import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-empty',
  template: `
    <div class="loader-empty" *ngIf="isVisible">
      <nz-empty [nzNotFoundContent]="description"></nz-empty>
    </div>`,
  styles: [
    `.loader-empty {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
      text-align: center;
      background: rgba(0, 0, 0, 0.05);
      border-radius: 4px;
      margin-bottom: 20px;
      padding: 30px 50px;
      margin: auto;
    }

    :host::ng-deep .ant-empty-description {
      color: black;
    }
    `
  ]
})

export class EmptyComponent implements OnInit {

  @Input() isVisible: boolean;
  @Input() description: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
