import {Component, Input, OnInit} from '@angular/core';
import {config} from '../../../app.config';

@Component({
  selector: 'app-serie-cast-card',
  template: `
    <nz-card nzHoverable [nzCover]="!loader ? coverTemplate : coverScheleton">
      <ng-template #coverTemplate>
        <img
          [src]="castDetails.profile_path ? imgBaseUrl + castDetails.profile_path : imageDefaultUrlCard">
      </ng-template>
      <nz-card-meta
        class="title"
        [nzTitle]="!loader ? castDetails.original_name : ''"
      >
      </nz-card-meta>
      <nz-skeleton
        [nzLoading]="loader"
        [nzTitle]="{ rows: 1, width:100 }"
        [nzParagraph]="false"
        [nzActive]="true">
        <ng-template #coverScheleton>
          <img alt="logo" [src]="imageDefaultUrlCard"/>
        </ng-template>
      </nz-skeleton>
    </nz-card>
  `,
  styles: [`
    nz-card {
      border-radius: 20px;
      margin-bottom: 20px;
    }

    img {
      border-top-left-radius: 20px;
      border-top-right-radius: 20px;
      padding: 5px;
    }
  `]
})
export class SerieCastCardComponent implements OnInit {
  @Input() castDetails: any;
  @Input() loader: boolean;
  imgBaseUrl = config.imageBaseUrl;
  imageDefaultUrlCard = config.imageDefaultUrlCard;

  constructor() {
  }

  ngOnInit() {
  }

}
