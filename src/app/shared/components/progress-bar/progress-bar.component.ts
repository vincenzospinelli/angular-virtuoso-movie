import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  template: `
    <nz-progress
      [nzPercent]="percent"
      nzType="circle"
      [nzWidth]="width"
      [nzStrokeColor]="color"
      [ngClass]="classText">
    </nz-progress>`
})
export class ProgressBarComponent implements OnInit {

  @Input() percent: number;
  @Input() width: number;
  @Input() color = 'green';
  @Input() classText: 'white-text' | 'black-text' = 'black-text';

  constructor() {
  }

  ngOnInit() {
  }

}
