import {Component, Input, OnInit} from '@angular/core';
import {MovieDetailsModel} from '../../../models/movie-details.model';
import {config} from '../../../app.config';

@Component({
  selector: 'app-movie-card-details',
  template: `
    <nz-card nzHoverable
             [nzCover]="!loader ? coverTemplate : coverScheleton"
             [nzActions]="[calendar,heart]"
             [nzBodyStyle]="{display: 'none'}">
      <ng-template #coverTemplate>
        <img [src]="movieDetails.poster_path ? imgBaseUrl + movieDetails.poster_path : imageDefaultUrlCard">
      </ng-template>
      <nz-card-meta [nzTitle]="!loader ? movieDetails.title : ''">
      </nz-card-meta>
      <nz-skeleton
        [nzLoading]="loader"
        [nzTitle]="{ rows: 1, width:150 }"
        [nzParagraph]="false"
        [nzAvatar]="{ shape:'circle' }"
        [nzActive]="true">
        <ng-template #coverScheleton>
          <img alt="logo" [src]="imageDefaultUrlCard"/>
        </ng-template>
      </nz-skeleton>
    </nz-card>

    <ng-template #calendar>
      <i nz-icon nzType="calendar" nzTheme="twotone"></i>
      {{movieDetails && movieDetails.release_date ? (movieDetails.release_date | date: 'dd/MM/YYYY') : 'MESSAGGI.NESSUNA_VOTAZIONE' | translate}}
    </ng-template>
    <ng-template #heart>
      <i nz-icon nzType="heart" nzTheme="twotone" [nzTwotoneColor]="'red'"></i>
      {{movieDetails?.popularity | thousandSuff}}
    </ng-template>
  `,
  styles: [`
    nz-card {
      border-radius: 20px;
    }

    img {
      border-top-left-radius: 20px;
      border-top-right-radius: 20px;
      padding: 5px;
    }

    :host::ng-deep .ant-card-actions {
      border-bottom-left-radius: 20px;
      border-bottom-right-radius: 20px;
    }
  `]
})

export class MovieCardDetailsComponent implements OnInit {
  @Input() movieDetails: MovieDetailsModel;
  @Input() loader: boolean;
  @Input() progressWidth: number;

  imgBaseUrl: string = config.imageBaseUrl;
  imageDefaultUrlCard: string = config.imageDefaultUrlCard;

  constructor() {
  }

  ngOnInit() {
  }
}
