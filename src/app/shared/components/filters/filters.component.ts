import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-filters',
  template: `
    <div nz-row>
      <nz-card>
        <nz-row nzAlign="middle" nzGutter="24">
          <nz-col nzXs="24" nzSm="24" nzMd="24" nzLg="12" nzXl="12">
            <ng-content></ng-content>
          </nz-col>
          <nz-col nzXs="24" nzSm="24" nzMd="24" nzLg="12" nzXl="12">
            <nz-pagination class="pagination"
                           *ngIf="page && pageSize && totalResults"
                           [nzPageIndex]="page"
                           [nzPageSize]="pageSize"
                           [nzTotal]="totalResults"
                           [nzSize]="sizePaginator"
                           [nzResponsive]="true"
                           (nzPageIndexChange)="onChange($event)">
            </nz-pagination>
          </nz-col>
        </nz-row>
      </nz-card>
    </div>
  `,
  styles: [`
    nz-card {
      width: 100%;
      margin-bottom: 24px;
      border-radius: 10px;
    }

    .pagination {
      float: right;
      padding: 24px;
    }
  `]
})

export class FiltersComponent implements OnInit {

  @Input() page: number;
  @Input() pageSize: number;
  @Input() totalResults: number;
  @Input() sizePaginator: string;
  @Output() onPageIndexChange: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onChange(event) {
    this.onPageIndexChange.emit(event);
  }

}
