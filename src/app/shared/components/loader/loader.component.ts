import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-loader',
  template: `
    <div class="loader-container" *ngIf="loader">
      <nz-spin nzSimple [nzSpinning]="loader" [nzSize]="'large'"></nz-spin>
    </div>`,
  styles: [
    `.loader-container {
      display: flex;
      justify-content: center;
      align-items: center;
      height: 100vh;
      text-align: center;
      background: rgba(0, 0, 0, 0.05);
      border-radius: 4px;
      margin-bottom: 20px;
      padding: 30px 50px;
      margin: auto;
    }`
  ]
})

export class LoaderComponent implements OnInit {

  @Input() loader: boolean;

  constructor() {
  }

  ngOnInit(): void {
  }

}
