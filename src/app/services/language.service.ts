import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class LanguageService {
  private isChange: Subject<boolean> = new Subject<boolean>();

  constructor() {
  }

  sendMessage(isChange: boolean) {
    this.isChange.next(isChange);
  }

  getMessage(): Observable<any> {
    return this.isChange.asObservable();
  }
}
