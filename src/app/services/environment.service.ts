import {InjectionToken} from '@angular/core';
import {environment} from '../../environments/environment';

export const ENV = new InjectionToken<any>('env');

export function getEnv() {
  return environment;
}
