import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {MovieModel} from '../models/movie.model';
import {MovieDetailsModel} from '../models/movie-details.model';
import {CreditsModel} from '../models/credits.model';
import {config} from '../app.config';
import {ENV} from './environment.service';

@Injectable()
export class TheMovieDbService {

  private apiBaseUrl: string = config.apiBaseUrl;
  private apiVersion: string = this.env.apiVersion;
  private apiReadAccessToken: string = this.env.apiReadAccessToken;
  private httpHeaders = new HttpHeaders();
  private searchParameters: HttpParams;

  constructor(
    @Inject(ENV) private env,
    private http: HttpClient
  ) {
  }

  getPopularMovie(page: string, lang: string) {
    const params = new HttpParams()
      .set('page', page)
      .set('language', lang);

    return this.http.get(`${this.apiBaseUrl + this.apiVersion}/movie/popular`, {
      headers: this.httpHeaders.append('authorization', 'Bearer ' + this.apiReadAccessToken),
      params
    });
  }

  getDiscoverMovie(pageNo: number, sortOn: string, lang: string): Observable<MovieModel[]> {
    const params = new HttpParams()
      .set('page', pageNo.toString())
      .set('sort_by', sortOn)
      .set('language', lang);

    return this.http.get<MovieModel[]>(`${this.apiBaseUrl + this.apiVersion}/discover/movie`, {
      headers: this.httpHeaders.append('authorization', 'Bearer ' + this.apiReadAccessToken),
      params
    });
  }

  getMovieDetailsById(id: number, lang: string): Observable<MovieDetailsModel> {
    const params = new HttpParams()
      .set('language', lang);

    return this.http.get<MovieDetailsModel>(`${this.apiBaseUrl + this.apiVersion}/movie/${id}`, {
      headers: this.httpHeaders.append('authorization', 'Bearer ' + this.apiReadAccessToken),
      params
    });
  }

  getMovieCreditsById(id: number, lang: string): Observable<CreditsModel> {
    const params = new HttpParams()
      .set('language', lang);

    return this.http.get<CreditsModel>(`${this.apiBaseUrl + this.apiVersion}/movie/${id}/credits`, {
      headers: this.httpHeaders.append('authorization', 'Bearer ' + this.apiReadAccessToken),
      params
    });
  }

  getWatchProviders(id: number): Observable<any> {
    return this.http.get<CreditsModel>(`${this.apiBaseUrl + this.apiVersion}/movie/${id}/watch/providers`, {
      headers: this.httpHeaders.append('authorization', 'Bearer ' + this.apiReadAccessToken)
    });
  }

  searchMovie(query: string, lang: string, page: number): Observable<any> {
    if (query === '') {
      return null;
    }

    const params = new HttpParams()
      .set('query', query)
      .set('language', lang)
      .set('page', page.toString());


    return this.http.get<CreditsModel>(`${this.apiBaseUrl + this.apiVersion}/search/movie`, {
      headers: this.httpHeaders.append('authorization', 'Bearer ' + this.apiReadAccessToken),
      params
    });
  }
}
