import {Component, Inject, OnInit} from '@angular/core';
import {gridResponsiveMap, NzBreakpointEnum, NzBreakpointService} from 'ng-zorro-antd/core/services';
import {TranslateService} from '@ngx-translate/core';
import {en_US, it_IT, NzI18nService} from 'ng-zorro-antd/i18n';
import {LanguageService} from './services/language.service';
import {ENV} from './services/environment.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isCollapsed: boolean = false;
  className: string;
  languages: string[] = [];
  lang: string = 'it';

  constructor(
    @Inject(ENV) private env,
    private breakpointService: NzBreakpointService,
    private translate: TranslateService,
    private languageService: LanguageService,
    private i18n: NzI18nService
  ) {
    translate.setDefaultLang('it');
    translate.addLangs(['it', 'en']);
    translate.use(localStorage.getItem('lang') ? localStorage.getItem('lang') : 'it');
  }

  ngOnInit(): void {
    console.log(`Starting in ${this.env.name} mode`);

    this.languages = this.translate.getLangs();
    this.lang = this.translate.currentLang;
    this.breakpointService.subscribe(gridResponsiveMap).subscribe((bp: NzBreakpointEnum) => {
      switch (bp) {
        case NzBreakpointEnum.md:
        case NzBreakpointEnum.sm:
        case NzBreakpointEnum.xs:
          this.className = 'logo-xs-sm-md';
          break;
        case NzBreakpointEnum.lg:
        case NzBreakpointEnum.xl:
        case NzBreakpointEnum.xxl:
          this.className = 'logo-xl-xxl-lg';
          break;
      }
    });
  }

  setLanguage(lang: string) {
    this.lang = lang;
    this.translate.use(lang);
    this.i18n.setLocale(lang === 'it' ? it_IT : en_US);
    this.languageService.sendMessage(true);
    localStorage.setItem('lang', lang);
  }
}
