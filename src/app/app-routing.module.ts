import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './pages/errors/page-not-found/page-not-found.component';
import {PageSystemErrorComponent} from './pages/errors/page-system-error/page-system-error.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/dashboard'},
  {path: 'dashboard', loadChildren: () => import('./pages/features/dashboard/dashboard.module').then(m => m.DashboardModule)},
  {path: '404', component: PageNotFoundComponent},
  {path: '500', component: PageSystemErrorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
