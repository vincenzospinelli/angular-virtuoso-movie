import {CastModel} from './cast.model';

export interface CreditsModel {
  cast: Array<CastModel>;
  crew: Array<any>;
  id: number;
}
