# Healthy Virtuoso - Frontend Engineer coding exercise

## Introduction 

First of all, thank you for your interest in working with us and for taking the time to go through this little coding
exercise, ​we really appreciate it​. The main goal of this exercise is to help us understand how you usually approach,
organize and solve some common challenges that affect every single Frontend Engineer. An important note before moving on
to the details of the exercise: anything that has not been specifically requested or mentioned inside this document (
e.g. application architecture, dependencies ecc) can be handled by the developer according to his/her preferences.

## The exercise

Create a simple single page web application using ​The Movie Database API​ displaying a list of movies that can be
selected to view more details.

## The app requirements

Display a list of movies (​hint: you can use the ​/movie/popular​ endpoint​) and make it sortable either by release date
or vote average. The details view shows the movie’s: poster, title, plot overview.

## The code requirements

The app must be developed using one of the following frameworks: Angular, React or Vue.js. The app must be structured as
if this is just the first step of a much larger project - organize the structure accordingly

That’s all, to help us move quickly with the hiring process we ask you to send us your solution ​within a week u​ sing
the way you prefer (a link to a public repository, a shared folder, a zip or any other quick and secure way is welcome),
for that reason if you have any blocking questions regarding this coding exercise, email us at
developers@healthyvirtuoso.com​ and we’ll get back to you as soon as possible.
